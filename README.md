# WebSocket Chat
It's implemented chat that can broadcast messages between 2 or more different windows as it is shown on the screenshot bellow.

![Screenshot_2022-01-31_at_20.10.48](/uploads/841616135d960a1e2dfb6b8cad157f74/Screenshot_2022-01-31_at_20.10.48.png)

![Screenshot_2022-01-31_at_20.10.44](/uploads/f71ac45fb56d6fa1a85f531f97dfc2b0/Screenshot_2022-01-31_at_20.10.44.png)

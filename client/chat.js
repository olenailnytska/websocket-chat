const chat = document.getElementById('chat');
const form = document.getElementById('message-form');
let fullName = prompt('Enter your name');

const ws = new WebSocket('ws://localhost:8080');

ws.onmessage = (message) => {
  let data = JSON.parse(message.data);

  const div = document.createElement('div');
  div.classList.add('message-block');
  
  const nameBlock = document.createElement('div');
  nameBlock.innerHTML = data.name;
  nameBlock.classList.add('name');
  
  const messageBlock = document.createElement('div');
  messageBlock.innerHTML = data.message;
  messageBlock.classList.add('message');
  
  const timeBlock = document.createElement('div');
  timeBlock.innerHTML = data.time;
  
  div.appendChild(nameBlock);
  div.appendChild(messageBlock);
  div.appendChild(timeBlock);
  chat.appendChild(div);
}

const sendMessage = (event) => {
  event.preventDefault();
  const message = document.getElementById('message').value;
  document.getElementById('message').value = '';
  
  const date = new Date();
  let hours = date.getHours();
  let timeFormat = 'AM';
  let ten = 10;
  let twelve = 12;
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();
  
  if(hours<ten) {
    hours = '0' + hours;
  }
  
  if(hours===twelve) {
    timeFormat = 'PM';
  }
  
  if(hours>twelve) {
    hours = hours - twelve;
    timeFormat = 'PM';
  }
  
  if(minutes < ten) {
    minutes = '0' + minutes;
  }
  
  if(seconds < ten) {
    seconds = '0' + seconds;
  }
  let dateString = hours + ':' + minutes + ':' + seconds + ' ' + timeFormat;

  const div = document.createElement('div');
  div.style.textAlign = 'right';
  div.classList.add('message-block');
  
  const nameBlock = document.createElement('div');
  nameBlock.innerHTML = fullName;
  nameBlock.classList.add('name');
  
  const messageBlock = document.createElement('div');
  messageBlock.innerHTML = message;
  messageBlock.classList.add('message');
  
  const timeBlock = document.createElement('div');
  timeBlock.innerHTML = dateString;
  
  div.appendChild(nameBlock);
  div.appendChild(messageBlock);
  div.appendChild(timeBlock);
  chat.appendChild(div);

  ws.send(JSON.stringify({
    name: fullName,
    message, 
    time: dateString
  }));
}

form.addEventListener('submit', sendMessage);